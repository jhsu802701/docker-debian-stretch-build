# Building Docker Images

This is a repository for building Docker images.  

## Prerequisites
* Docker should be installed.
* You should use the nuke.sh command in one of the docker-debian-stretch repositories to clear all Docker images and containers. Because this disrupts all other Docker activity, it is recommended that you build Docker images on a dedicated physical or virtual machine.

## Building Images
Download this repository, use the "cd" command to enter the directory for the Docker image you wish to build. Run the "build.sh" script.

## Docker Hub
All Docker images built with this repository are produced on Docker Hub using the automated build procedure.  However, running the build process on your local machine allows you to experiment without putting bad images on Docker Hub.

